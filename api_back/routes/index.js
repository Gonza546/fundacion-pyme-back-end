const usuarioController = require('../controllers/usuario');
const encuestasController = require ('../controllers/encuestas');
const seccionesController = require('../controllers/secciones');
const preguntasController = require ('../controllers/preguntas');
const respuestasController = require ('../controllers/respuestas');
const comentariosController=require('../controllers/comentarios');
const importController = require('../controllers/import');



const Authorization = require('../auth/authorization');

module.exports = (app) => {
    app.get('/api', (req,res) => res.status(200).send({
        message: "Dentro de la api",
    }));

    //Users
    app.post('/api/user/create', Authorization,usuarioController.create);
    app.get('/api/user/list', Authorization ,usuarioController.list);
    app.post('/api/user/find', Authorization,usuarioController.find);
    app.post('/api/user/modify',Authorization, usuarioController.modify);
    app.post('/api/user/delete',Authorization, usuarioController.delete);
    //Login
    app.post('/api/login', usuarioController.login);//(no securizado)

    // Muestra todas las encuestas para la pantalla listadoEncuestas
    app.post('/api/respuestas/find',Authorization,respuestasController.buscarRespuestaporidPregunta);
    app.patch('/api/respuestas/update',Authorization,respuestasController.actualizarRespuesta);
    app.post('/api/encuestas/list',Authorization,encuestasController.listarTodasLasEncuesta);
    app.get('/api/encuestas/find',Authorization,encuestasController.listarEncuestaPorid);
    app.post('/api/preguntas/find', Authorization,preguntasController.buscarPreguntasPoridEncuesta);
    app.post('/api/comentarios/find',Authorization,comentariosController.buscarComentariosPoridEncuesta);
    app.post('/api/comentarios/findIdPreg',Authorization,comentariosController.buscarComentariosPoridPregunta);
    app.post('/api/comentarios/create',Authorization,comentariosController.create);
    app.get('/api/import', importController.import);
    app.patch('/api/preguntas/aprobar',Authorization,preguntasController.aprobarPregunta);
    app.patch('/api/preguntas/rechazar', Authorization,preguntasController.rechazarPregunta);
    app.patch('/api/preguntas/devolver', Authorization,preguntasController.devolverPregunta);
    app.patch('/api/encuestas/analista',Authorization,encuestasController.pendienteAnalistaEncuesta);
    app.post('/api/encuestas/pendienteAprobador',Authorization,encuestasController.encuestaAPendienteAprobador);
    app.post('/api/encuestas/Aprobada',Authorization,encuestasController.encuestaAprobada);
    app.post('/api/encuestas/pendienteAnalista',Authorization,encuestasController.encuestaARevision);

};
