'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('respuestas', {
      idRespuesta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idPregunta: {
        type: Sequelize.INTEGER,
        references:{
          model: 'preguntas',
          key: 'idPregunta'
        }
      },
      respuesta: {
        type: Sequelize.CHAR
      },
      esActualizada: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('respuestas');
  }
};