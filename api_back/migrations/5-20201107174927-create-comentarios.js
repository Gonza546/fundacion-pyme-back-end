'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('comentarios', {
      idComentario: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idPregunta: {
        type: Sequelize.INTEGER,
        references:{
          model: 'preguntas',
          key:'idPregunta'
        }
      },
      comentario: {
        type: Sequelize.STRING
      },
      fecha: {
        type: Sequelize.DATE
      },
      idUsuario: {
        type: Sequelize.INTEGER,
        references:{
          model: 'usuarios',
          key:'id'
        }
      },
      respuestaActualizada: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('comentarios');
  }
};