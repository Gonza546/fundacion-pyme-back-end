'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('preguntas', {
      idPregunta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      idSeccion: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references:{
          model: 'secciones',
          key:'idSeccion'
        }
      },
      idEncuesta: {
        type: Sequelize.INTEGER,
        references:{
          model: 'encuestas',
          key:'idEncuesta'
        }
      },
      idPreguntaPadre: {
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      type: {
        type: Sequelize.STRING
      },
      titulo: {
        type: Sequelize.STRING
      },
      mandatory: {
        type: Sequelize.BOOLEAN
      },
      valoresPosibles: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('preguntas');
  }
};