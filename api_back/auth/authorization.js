var jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];

var authorization = function (req, res, next) {

    var token = req.headers['x-access-token'];
    console.log("token", token);
    var msg = { auth: false, message: 'Se requiere token de sesion' };
    if (!token) {
        res.status(500).send(msg);
    }

    let sec = config.secret;
    //console.log("secret",sec)
    jwt.verify(token, sec, function (err, decoded) {
        var msg = { auth: false, message: 'Error al validar el token de sesion' };
        if (err) {
            res.status(500).send(msg);
        }
        req.userId = decoded.id;
        next();
    });
}

module.exports = authorization;

