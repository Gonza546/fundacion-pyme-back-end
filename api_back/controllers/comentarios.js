const Sequelize = require('Sequelize');
const { create } = require('./usuario');
const preguntas = require('../models').preguntas;
const usuarios = require('../models').usuario;
const comentarios = require('../models').comentarios;

module.exports = {

    buscarComentariosPoridEncuesta(req, res) {
        return comentarios.findAll({
            include: [{model: usuarios,as: 'usuario',attributes:['nombre','rol']},
            {
                //tengo que hacer join con preguntas y usuarios como tengo en el workbench
            model:preguntas,
            as:'preguntas',where: {idEncuesta: req.body.idEncuesta},
            attributes:['idEncuesta']
            }],
            attributes:[
                'idComentario',
                'idPregunta',
                'comentario',
                'idUsuario',
                'createdAt'
            ],
            order: [
                ['createdAt','ASC']
            ]
        })
            .then(comentarios => res.status(200).send(comentarios))
            .catch(error => res.status(400).send(error))
    },
    buscarComentariosPoridPregunta(req, res) {
        return comentarios.findAll({
            include: [{model: usuarios,as: 'usuario',attributes:['nombre','rol']},
            {
                //tengo que hacer join con preguntas y usuarios como tengo en el workbench
            model:preguntas,
            as:'preguntas',where: {idEncuesta: req.body.idPregunta},
            attributes:['idPregunta']
            }],
            attributes:[
                'idComentario',
                'comentario',
                'idUsuario',
                'createdAt'
            ],
            order: [
               ['createdAt','ASC']
            ]
        })
            .then(comentarios => res.status(200).send(comentarios))
            .catch(error => res.status(400).send(error))
    },
    create(req,res){
        return comentarios.create({
            idPregunta:req.body.idPregunta,
            idUsuario:req.body.idUsuario,
            comentario:req.body.comentario
        })
        .then(comentarios => res.status(200).send(comentarios))
        .catch(error => res.status(400).send(error))
    }
};