const encuestasController = require ('./encuestas');
const seccionesController = require('./secciones');
const preguntasController = require ('./preguntas');
const respuestasController = require ('./respuestas');
const comentariosController = require('./comentarios');
const importController = require('./import');

module.export={
    encuestasController,
    seccionesController,
    preguntasController,
    respuestasController,
    comentariosController,
    importController
}