const Sequelize = require('Sequelize');
const preguntas = require('../models').preguntas;
const respuestas = require('../models').respuestas;

module.exports = {

    buscarRespuestaporidPregunta(req,res){
        return respuestas.findAll({
            include:[{
                model: preguntas,
                as:'preguntas',
                where:{
                    idEncuesta:req.body.idEncuesta
                },
                attributes:[
                    'idEncuesta'
                ]
            }],
            attributes:[
                
                'idRespuesta',
                'idPregunta',
                'respuesta',
                'esActualizada'
            ]
        })
        .then(respuestas => res.status(200).send(respuestas))
        .catch(error => res.status(400).send(error))
    },
    actualizarRespuesta(req,res){
        return respuestas.update({respuesta:req.body.rtaActualizada},{
            where:{
                //respuesta:req.body.respuestaVieja,
                idPregunta:req.body.idPregunta,
                esActualizada:1  
            }
        })
        .then(respuestas => res.status(200).send(respuestas))
        .catch(error => res.status(400).send(error))
    }

}