const axios = require('axios');


module.exports = {
    importarEncuestas(url, res) {
        return axios.get(
            url,
            {
                headers: { 'x-api-key': '5CD4ED173E1C95FE763B753A297D5' },
            }
        )
            //.then( axios => axios )
            .then(axios => axios)
            .catch(error => res.status(400).send(error))
    }
}