const Sequelize = require('sequelize');
const usuario = require('../models').usuario;
var UserService = require('../services/user.js');

module.exports = {

    //Create User
    async create(req, res) {
        //console.log(req.body);
        var User = {
            nombre: req.body.fullname,
            email: req.body.email,
            contrasena: req.body.password,
            rol: req.body.role,
            estado: 'ACTIVO'
        }
        try {
            var createdUser = await UserService.createUser(User);
            return res.status(200).json({ status: 200, token: createdUser, message: "Usuario creado correctamente" });
        } catch (error) {
            return res.status(400).json({ status: 400, message: "Error: el usuario no pudo ser creado" });
        }
    },
    //Login User
    async login(req, res) {
        //console.log(req.body);
        var User = {
            email: req.body.email,
            contrasena: req.body.password,
        }
        try {
            var loggedInUser = await UserService.loginUser(User);
            console.log("usuario: autenticado ok");
            return res.status(200).json({ status: 200, loggedInUser, message: "Usuario logeado correctamente" });
        } catch (error) {
            console.log("error");
            return res.status(400).json({ status: 400, message: "Error: el usuario no pudo ser autenticado" });
        }
    },
    //List all active users
    list(_, res) {
        return usuario.findAll({
            where: {
                estado: 'ACTIVO'
            }
        })
            .then(usuario => res.status(200).json({ status: 200, usuario }))
            .catch(error => res.status(400).json({ status: 400, error }))
    },
    //List user by email
    find(req, res) {
        return usuario.findOne({
            where: {
                email: req.body.email,
            },
            attributes:[
                'id'
            ]
        })
            .then(usuario => res.status(200).send(usuario))
            .catch(error => res.status(400).send(error))
    },
    //Modify user by email
    modify(req, res) {
        console.log(req.body);
        return usuario.update(
            {
                nombre: req.body.fullname,
                contrasena: req.body.password,
                rol: req.body.role
            },
            { where: { email: req.body.email } }
        )
            .then(function (usuario) {
                res.status(200).send();
            })
            .catch(error => res.status(400).send(error))
    },
    //Delete user by email (logical)
    delete(req, res) {
        console.log(req.body);
        return usuario.update(
            {
                estado: 'INACTIVO',
            },
            { where: { email: req.body.email } }
        )
            .then(function (usuario) {
                res.status(200).json({ status: 200 })
            })
            .catch(error => res.status(400).json({ status: 400, error }))
    },
};