const Sequelize = require('Sequelize');
const encuestas = require('../models').encuestas;
const preguntas = require('../models').preguntas;
const { Op } = require('sequelize');

module.exports = {

    //Recibir una encuesta y agregarla a la base de datos
    crearEncuestaRecibida(req, res) {
        return encuestas.findOrCreate({
            where: {

                pyme: req.body.pyme,
                titulo: req.body.titulo,
                description: req.body.description,
                status: req.body.status
            },
            pyme: req.body.pyme,
            titulo: req.body.titulo,
            description: req.body.description,
            status: req.body.status

        })
            .then(encuestas => res.status(200).send(encuestas))
            .catch(error => res.status(400).send(error))

    },

    //Listar todas las encuestas
    listarTodasLasEncuesta(req, res) {
        var query;
        if (req.body.status != 'Todo') {
            query = encuestas.findAll({
                where: {
                    status: req.body.status
                },
                order: [
                    ['titulo', 'ASC']
                ]
            })
        }
        else {
            query = encuestas.findAll({

                order: [
                    ['titulo', 'ASC']
                ]
            });
        }
        return query
            .then(encuestas => res.status(200).send(encuestas))
            .catch(error => res.status(400).send(error))
    },

    //La idea es que busque por idEncuesta
    listarEncuestaPorid(req, res) {
        return encuestas.findAll({
            where: {
                idEncuesta: req.body.idEncuesta
            }
        })
            .then(encuestas => res.status(200).send(encuestas))
            .catch(error => res.status(400).send(error))
    },

    pendienteAnalistaEncuesta(req, res) {
        return encuestas.update({ status: 'Pendiente Analista' }, {
            where: { idEncuesta: req.body.idEncuesta }
        })
            .then(encuestas => res.status(200).send(encuestas))
            .catch(error => res.status(400).send(error))
    },

    //Cambia encuesta a "Pendiente Aprobador"
    async encuestaAPendienteAprobador(req, res) {
        var encuestaAPendienteAprobador = false;
        //Valida primero que no haya ninguna pregunta pendiente de responder por el analista
        try {
            var preguntasDB = await preguntas.count({
                where: {
                    status: 'Pendiente Analista'
                },
                include: [{
                    model: encuestas,
                    as: 'encuestas',
                    where: {
                        idEncuesta: req.body.idEncuesta
                    }
                }]
            });
            console.log(preguntasDB)
            if (preguntasDB == 0) {
                var actualizoEncuestaDB = await encuestas.update({
                    status: 'Pendiente Aprobador'
                }, {
                    where: { idEncuesta: req.body.idEncuesta }
                });
                encuestaAPendienteAprobador = true;
                return res.status(200).json({ status: 200, encuestaAPendienteAprobador, message: "Encuesta actualizada" });
            }
            else {
                return res.status(200).json({ status: 400, encuestaAPendienteAprobador, message: "Existen preguntas pendientes de responder" });
            }

        } catch (error) {
            console.log("error: " + error);
            return res.status(400).json({ status: 400, message: "Error: No se pudo actualizar la encuesta" });
        }
    },

    async encuestaAprobada(req, res) {
        var encuestaAprobada = false;
        //Valida primero que no haya ninguna pregunta pendiente de responder por el aprobador
        try {
            var preguntasDB = await preguntas.count({
                where: {
                    [Op.or]: [
                        { status: 'Pendiente Validación' },
                        { status: 'Pendiente Analista' }
                    ]
                },
                include: [{
                    model: encuestas,
                    as: 'encuestas',
                    where: {
                        idEncuesta: req.body.idEncuesta
                    }
                }]
            });
            console.log(preguntasDB)
            if (preguntasDB == 0) {
                var actualizoEncuestaDB = await encuestas.update({
                    status: 'Aprobada'
                }, {
                    where: { idEncuesta: req.body.idEncuesta }
                });
                encuestaAprobada = true;
                return res.status(200).json({ status: 200, encuestaAprobada, message: "Encuesta aprobada" });
            }
            else {
                return res.status(200).json({ status: 400, encuestaAprobada, message: "Existen preguntas pendientes de aprobar" });
            }

        } catch (error) {
            console.log("error: " + error);
            return res.status(400).json({ status: 400, message: "Error: No se pudo aprobar la encuesta" });
        }

    },

    async encuestaARevision(req, res) {
        var encuestaARevision = false;
        //Valida primero que no haya ninguna pregunta pendiente de responder por el aprobador
        try {
            var preguntasDBV = await preguntas.count({
                where: {
                    status: 'Pendiente Validación'
                },
                include: [{
                    model: encuestas,
                    as: 'encuestas',
                    where: {
                        idEncuesta: req.body.idEncuesta
                    }
                }]
            });
            console.log("preguntas pendientes por el aprobador: " + preguntasDBV)
            //Veo si hay preguntas para el analista
            var preguntasDBA = await preguntas.count({
                where: {
                    status: 'Pendiente Analista'
                },
                include: [{
                    model: encuestas,
                    as: 'encuestas',
                    where: {
                        idEncuesta: req.body.idEncuesta
                    }
                }]
            });
            console.log("preguntas pendientes para el analista: " + preguntasDBA)

            if (preguntasDBV == 0 && preguntasDBA > 0) {
                var actualizoEncuestaDB = await encuestas.update({
                    status: 'Pendiente Analista'
                }, {
                    where: { idEncuesta: req.body.idEncuesta }
                });
                encuestaARevision = true;
                return res.status(200).json({ status: 200, encuestaARevision, message: "Encuesta enviada a Analista" });
            }
            //Aqui deberia dar un mensaje de que estan todas las preguntas OK, no hay nada que mandar a revision
            else if (preguntasDBV == 0) {
                return res.status(200).json({ status: 400, encuestaARevision, message: "Debe haber al menos una pregunta para el analista" });
            }
            //Aqui deberia dar un mensaje de que existen preguntas pendientes a validar
            else {
                return res.status(200).json({ status: 400, encuestaARevision, message: "Debe aprobar/rechazar las preguntas antes de enviarlas" });
            }

        } catch (error) {
            console.log("error: " + error);
            return res.status(400).json({ status: 400, message: "Error: No se pudo enviar la encuesta" });
        }

    },


}