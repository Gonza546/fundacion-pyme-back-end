const Sequelize = require('Sequelize');
const modeloencuesta = require('../models').encuestas;
const modeloseccion = require('../models').secciones;
const modelopregunta = require('../models').preguntas;
const modelorespuesta = require('../models').respuestas;

const controllerencuestas = require ('.').encuestas;


const auxiliar = require('./auxiliar');

const axios = require('axios');
//const url = 'http://localhost:8080/external-api/polls' //mockmio
const url = 'https://observatorio-pyme-answer-back.herokuapp.com/external-api/polls/' //url real


module.exports = {
    import(req, res) {
        
        const fecha=req.query.fecha;
        const urlCompleta = url+'?date='+fecha;

        return auxiliar.importarEncuestas(urlCompleta)
            .then(importacion => {

                encuestas = importacion.data;          
                encuestas.forEach((enc) => {
                    CrearEncuesta(enc)
                        .then((encuestaCreada => {
                            enc.sections.forEach(sec => {
                                CrearSeccion(encuestaCreada.idEncuesta, sec)
                                    .then(seccionCreada => {
                                        sec.questions.forEach(question => {
                                            CrearPregunta(encuestaCreada.idEncuesta, seccionCreada.idSeccion, question, 0)
                                            //nada?
                                        })
                                    })
                            })
                        }))

                })
                    res.status(200).send(String('Import OK.'))
            })
            .catch(error => res.status(400).send(error))
    }
};


function CrearEncuesta(enc) {
    return encuesta = modeloencuesta.create({
        pyme: enc.company.name,
        titulo: enc.name,
        description: enc.description,
        status: 'Pendiente Aprobador'
    })
}

function CrearSeccion(idEnc, sec) {
    return seccion = modeloseccion.create({
        idEncuesta: idEnc,
        titulo: sec.title,
        description: sec.description,
    })
}

function CrearPregunta(idEnc, idSec, preg, idPregPadre) {
    modelopregunta.create({
        idEncuesta: idEnc,
        idSeccion: idSec,
        idPreguntaPadre: idPregPadre,
        type: transformarTipoPregunta(preg.type),
        titulo: preg.title,
        mandatory: preg.mandatory || 0,
        valoresPosibles: String(preg.options),
        status: 'Pendiente Validación'
    })
        .then(preguntaCreada => {
            //creo la rta original
            CrearRespuesta(preguntaCreada.idPregunta, preg.type, preg.value, preg.options, 0)
                .then(resultadoRespuesta => {
                    //nada?
                })
            //creo la rta actualizada
            CrearRespuesta(preguntaCreada.idPregunta, preg.type, preg.value, preg.options, 1)
            .then(resultadoRespuesta => {
                //nada?
            })
            //si questions no es null, es que es agrupadora
            if (preg.questions) {
                preg.questions.forEach((pregHijo) => {
                    CrearPregunta(idEnc, idSec, pregHijo, preguntaCreada.idPregunta)
                })
            }

        })
}

function CrearRespuesta(idPreg, tipoPreg, rta, valores, esAct) {
    return respuesta = modelorespuesta.create({
        idPregunta: idPreg,
        respuesta: armaRta(rta, tipoPreg, valores),
        esActualizada: esAct
    })
}


function transformarTipoPregunta(type) {
    switch (type) {
        case 'TEXT':
            tipo = 'texto';
            break;
        case 'NUMBER':
            tipo = 'numerica';
            break;
        case 'CHOICE':
            tipo = 'simple';
            break;
        case 'SELECT':
            tipo = 'multiple';
            break;
        case 'FILE':
            tipo = 'archivo';
            break;
        case 'GROUPED':
            tipo = 'agrupada';
            break;
    }
    return tipo;
}




function armaRta(resp, tipo, valores) {

    switch (tipo) {
        case 'CHOICE':
            devolver = valores[resp];
            break;
        case 'FILE':
            devolver = resp[0].name;
            break;
        //probar este, en el json del otro grupo no existe ningun caso
        case 'SELECT':
            if (typeof resp === 'undefined') {
                if (typeof resp === Array) {
                    devolver = (resp.map(indice => { return valores[indice] })).toString();
                }
                else {
                    devolver = valores[resp];
                }
            } else {
                devolver = '';
            }
            break;

        default:
            if (typeof resp === 'undefined') {
                devolver = '';
            }
            else {
                devolver = resp.toString();
            }
    }
    return devolver;

}