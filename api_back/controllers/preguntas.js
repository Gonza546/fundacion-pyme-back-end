const Sequelize = require('Sequelize');
const encuestas = require('../models').encuestas;
const preguntas = require ('../models').preguntas;

module.exports = {

crearPregunta(req,res){
    return preguntas.findOrCreate({
        where:{

            idEncuesta:req.body.idEncuesta,
            type: req.body.type,
            titulo:req.body.titulo,
            mandatory:req.body.mandatory,
            valoresPosibles: req.body.valoresPosibles,
            status:req.body.status
        },
        idEncuesta:req.body.idEncuesta,
        idSeccion:req.body.idSeccion,
        type: req.body.type,
        titulo:req.body.titulo,
        mandatory:req.body.mandatory,
        valoresPosibles: req.body.valoresPosibles,
        status:req.body.status

    })
    .then(preguntas => res.status(200).send(preguntas))
    .catch(error => res.status(400).send(error))

},

buscarPreguntasPoridEncuesta(req,res){
    
        return preguntas.findAll({
            where:{
                idEncuesta:req.body.idEncuesta
            },
            attributes:[
                'idEncuesta',
                'idPregunta',
                'idSeccion',
                'type',
                'titulo',
                'mandatory',
                'valoresPosibles',
                'status'
            ]

        })
        .then(preguntas => res.status(200).send(preguntas))
        .catch(error => res.status(400).send(error))
},

aprobarPregunta(req,res){
    return preguntas.update({status:'Aprobada'},{
        where:{idPregunta:req.body.idPregunta}
    })
    .then(preguntas => res.status(200).send(preguntas))
    .catch(error => res.status(400).send(error))
},

rechazarPregunta(req,res){
    return preguntas.update({status:'Pendiente Analista'},{
        where:{idPregunta:req.body.idPregunta}
    })
    .then(preguntas => res.status(200).send(preguntas))
    .catch(error => res.status(400).send(error))
},

devolverPregunta(req,res){
    return preguntas.update({status:'Pendiente Validación'},{
        where:{idPregunta:req.body.idPregunta}
    })
    .then(preguntas => res.status(200).send(preguntas))
    .catch(error => res.status(400).send(error))
}


}
