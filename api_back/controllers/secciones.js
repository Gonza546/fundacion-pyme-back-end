const Sequelize = require('Sequelize');
const secciones = require('../models').secciones;

module.exports = {

    buscarSeccionesPoridEncuesta(req,res){
    
        return secciones.findAll({
            where:{
                idEncuesta:req.body.idEncuesta
            },
            attributes:[
                'idEncuesta',
                'idSeccion',
                'titulo',
                'description',
            ]

        })
        .then(secciones => res.status(200).send(secciones))
        .catch(error => res.status(400).send(error))
}





}