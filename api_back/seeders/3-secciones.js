'use strict';
var bcrypt = require('bcryptjs');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('secciones', [

            //encuesta todas aprobadas
            {
                idSeccion: 1,
                idEncuesta: 1,
                titulo: 'Encuesta 2019',
                description: 'Encuesta del año 2019',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 2,
                idEncuesta: 2,
                titulo: 'Encuesta 2019',
                description: 'Encuesta del año 2019',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 3,
                idEncuesta: 3,
                titulo: 'Encuesta 2019',
                description: 'Encuesta del año 2019',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 4,
                idEncuesta: 4,
                titulo: 'Encuesta 2020 Primer Semestre',
                description: 'Encuesta del año 2019',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 5,
                idEncuesta: 5,
                titulo: 'Encuesta 2020 Primer Semestre',
                description: 'Encuesta del año 2019',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 6,
                idEncuesta: 6,
                titulo: 'Encuesta 2020 Primer Semestre',
                description: 'Encuesta del año 2019',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 7,
                idEncuesta: 7,
                titulo: 'Encuesta 2020 COVID',
                description: 'Resultados 2020 en pandemia',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idSeccion: 8,
                idEncuesta: 8,
                titulo: 'Encuesta 2020 COVID',
                description: 'Resultados 2020 en pandemia',
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ]);
    }
}