'use strict';
var bcrypt = require('bcryptjs');

module.exports = {
    up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('usuarios', [
        {
        nombre: 'Usuario Aprobador',
        email: 'aprobador@grupo4.com',
        contrasena: bcrypt.hashSync('aprobador', 8),
        rol: 'Aprobador',
        estado: 'ACTIVO',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'Usuario Analista',
        email: 'analista@grupo4.com',
        contrasena: bcrypt.hashSync('analista', 8),
        rol: 'Analista',
        estado: 'ACTIVO',
        createdAt: new Date(),
        updatedAt: new Date()
      },    
      {
        nombre: 'Usuario Administrador',
        email: 'administrador@grupo4.com',
        contrasena: bcrypt.hashSync('administrador', 8),
        rol: 'Administrador',
        estado: 'ACTIVO',
        createdAt: new Date(),
        updatedAt: new Date()
      }  
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
