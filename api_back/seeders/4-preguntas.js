'use strict';
var bcrypt = require('bcryptjs');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('preguntas', [

            //encuesta todas aprobadas
            //primer encuesta
            {
                idPregunta: 1,
                idSeccion: 1,
                idEncuesta: 1,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 2,
                idSeccion: 1,
                idEncuesta: 1,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 3,
                idSeccion: 1,
                idEncuesta: 1,
                type: 'simple',
                titulo: 'pregunta choice',
                mandatory: 1,
                valoresPosibles: 'Si,No,Tal Vez',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 4,
                idSeccion: 1,
                idEncuesta: 1,
                type: 'multiple',
                titulo: 'pregunta seleccion multiple',
                mandatory: 1,
                valoresPosibles: 'Primero,Segundo,Tercero',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 5,
                idSeccion: 1,
                idEncuesta: 1,
                type: 'archivo',
                titulo: 'ingrese un archivo',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //segunda encuesta
            {
                idPregunta: 6,
                idSeccion: 2,
                idEncuesta: 2,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 7,
                idSeccion: 2,
                idEncuesta: 2,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 8,
                idSeccion: 2,
                idEncuesta: 2,
                type: 'simple',
                titulo: 'pregunta choice',
                mandatory: 1,
                valoresPosibles: 'Si,No,Tal Vez',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 9,
                idSeccion: 2,
                idEncuesta: 2,
                type: 'multiple',
                titulo: 'pregunta seleccion multiple',
                mandatory: 1,
                valoresPosibles: 'Primero,Segundo,Tercero',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 10,
                idSeccion: 2,
                idEncuesta: 2,
                type: 'archivo',
                titulo: 'ingrese un archivo',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //tercer encuesta
            {
                idPregunta: 11,
                idSeccion: 3,
                idEncuesta: 3,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 12,
                idSeccion: 3,
                idEncuesta: 3,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 13,
                idSeccion: 3,
                idEncuesta: 3,
                type: 'simple',
                titulo: 'pregunta choice',
                mandatory: 1,
                valoresPosibles: 'Si,No,Tal Vez',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 14,
                idSeccion: 3,
                idEncuesta: 3,
                type: 'multiple',
                titulo: 'pregunta seleccion multiple',
                mandatory: 1,
                valoresPosibles: 'Primero,Segundo,Tercero',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 15,
                idSeccion: 3,
                idEncuesta: 3,
                type: 'archivo',
                titulo: 'ingrese un archivo',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //cuarta encuesta
            {
                idPregunta: 16,
                idSeccion: 4,
                idEncuesta: 4,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Pendiente Analista',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 17,
                idSeccion: 4,
                idEncuesta: 4,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Pendiente Analista',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 18,
                idSeccion: 4,
                idEncuesta: 4,
                type: 'simple',
                titulo: 'pregunta choice',
                mandatory: 1,
                valoresPosibles: 'Si,No,Tal Vez',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            //quinta encuesta
            {
                idPregunta: 19,
                idSeccion: 5,
                idEncuesta: 5,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 20,
                idSeccion: 5,
                idEncuesta: 5,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 21,
                idSeccion: 5,
                idEncuesta: 5,
                type: 'simple',
                titulo: 'pregunta choice',
                mandatory: 1,
                valoresPosibles: 'Si,No,Tal Vez',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            //sexta encuesta
            {
                idPregunta: 22,
                idSeccion: 6,
                idEncuesta: 6,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 23,
                idSeccion: 6,
                idEncuesta: 6,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 24,
                idSeccion: 7,
                idEncuesta: 7,
                type: 'simple',
                titulo: 'pregunta choice',
                mandatory: 1,
                valoresPosibles: 'Si,No,Tal Vez',
                status: 'Pendiente Validación',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //septima encuesta
            {
                idPregunta: 25,
                idSeccion: 7,
                idEncuesta: 7,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Pendiente Validación',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 26,
                idSeccion: 7,
                idEncuesta: 7,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Pendiente Validación',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //octava encuesta
            {
                idPregunta: 27,
                idSeccion: 8,
                idEncuesta: 8,
                type: 'texto',
                titulo: 'Primer pregunta, texto libre',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idPregunta: 28,
                idSeccion: 8,
                idEncuesta: 8,
                type: 'numerica',
                titulo: 'Pregunta numerica',
                mandatory: 1,
                valoresPosibles: '',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ]);
    }
}