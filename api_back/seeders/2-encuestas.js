'use strict';
var bcrypt = require('bcryptjs');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('encuestas', [

            //encuesta todas aprobadas
            {
                idEncuesta: 1,
                pyme: 'Pyme 1',
                titulo: 'Encuesta 2019',
                description: 'Encuesta del año 2019',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idEncuesta: 2,
                pyme: 'Pyme 2',
                titulo: 'Encuesta 2019',
                description: 'Encuesta del año 2019',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            }, {
                idEncuesta: 3,
                pyme: 'Pyme 3',
                titulo: 'Encuesta 2019',
                description: 'Encuesta del año 2019',
                status: 'Aprobada',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //encuestas en trabajo
            {   //pendiente analista
                idEncuesta: 4,
                pyme: 'Pyme 1',
                titulo: 'Encuesta 2020 Primer Semestre',
                description: 'Encuesta del año 2019',
                status: 'Pendiente Analista',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {   //pendiente aprobador con historial
                idEncuesta: 5,
                pyme: 'Pyme 2',
                titulo: 'Encuesta 2020 Primer Semestre',
                description: 'Encuesta del año 2020',
                status: 'Pendiente Aprobador',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {   //pendiente aprobador con historial
                idEncuesta: 6,
                pyme: 'Pyme 3',
                titulo: 'Encuesta 2020 Primer Semestre',
                description: 'Encuesta del año 2020',
                status: 'Pendiente Aprobador',
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //encuestas nueva
            {   //pendiente aprobador
                idEncuesta: 7,
                pyme: 'Pyme 1',
                titulo: 'Encuesta 2020 COVID',
                description: 'Resultados 2020 en pandemia',
                status: 'Pendiente Aprobador',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {   //pendiente aprobador
                idEncuesta: 8,
                pyme: 'Pyme 2',
                titulo: 'Encuesta 2020 COVID',
                description: 'Resultados 2020 en pandemia',
                status: 'Pendiente Aprobador',
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ]);
    }
}