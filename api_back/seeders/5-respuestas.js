'use strict';
var bcrypt = require('bcryptjs');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('respuestas', [

            //encuesta todas aprobadas
            //primer encuesta
            {
                idRespuesta: 1,
                idPregunta: 1,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 2,
                idPregunta: 1,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 3,
                idPregunta: 2,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 4,
                idPregunta: 2,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 5,
                idPregunta: 3,
                respuesta: 'Si',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 6,
                idPregunta: 3,
                respuesta: 'Si',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 7,
                idPregunta: 4,
                respuesta: 'Primero',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 8,
                idPregunta: 4,
                respuesta: 'Primero',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 9,
                idPregunta: 5,
                respuesta: 'https:\\link.com',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 10,
                idPregunta: 5,
                respuesta: 'https:\\link.com',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            //segunda encuesta
            {
                idRespuesta: 11,
                idPregunta: 6,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 12,
                idPregunta: 6,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 13,
                idPregunta: 7,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 14,
                idPregunta: 7,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 15,
                idPregunta: 8,
                respuesta: 'Si',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 16,
                idPregunta: 8,
                respuesta: 'Si',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 17,
                idPregunta: 9,
                respuesta: 'Primero',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 18,
                idPregunta: 9,
                respuesta: 'Primero',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 19,
                idPregunta: 10,
                respuesta: 'https:\\link.com',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 20,
                idPregunta: 10,
                respuesta: 'https:\\link.com',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //tercer encuesta
            {
                idRespuesta: 21,
                idPregunta: 11,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 22,
                idPregunta: 11,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 23,
                idPregunta: 12,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 24,
                idPregunta: 12,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 25,
                idPregunta: 13,
                respuesta: 'Si',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 26,
                idPregunta: 13,
                respuesta: 'Si',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 27,
                idPregunta: 14,
                respuesta: 'Primero',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 28,
                idPregunta: 14,
                respuesta: 'Segundo',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 29,
                idPregunta: 15,
                respuesta: 'https:\\link.com',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 30,
                idPregunta: 15,
                respuesta: 'https:\\link.com',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //cuarta encuesta
            {
                idRespuesta: 31,
                idPregunta: 16,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 32,
                idPregunta: 16,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 33,
                idPregunta: 17,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 34,
                idPregunta: 17,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 35,
                idPregunta: 18,
                respuesta: 'Si',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 36,
                idPregunta: 18,
                respuesta: 'No',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //quinta encuesta encuesta
            {
                idRespuesta: 37,
                idPregunta: 19,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 38,
                idPregunta: 19,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 39,
                idPregunta: 20,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 40,
                idPregunta: 20,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 41,
                idPregunta: 21,
                respuesta: 'Si',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 42,
                idPregunta: 21,
                respuesta: 'Si',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //sexta encuesta 
            {
                idRespuesta: 43,
                idPregunta: 22,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 44,
                idPregunta: 22,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 45,
                idPregunta: 23,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 46,
                idPregunta: 23,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 47,
                idPregunta: 24,
                respuesta: 'Si',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 48,
                idPregunta: 24,
                respuesta: 'Si',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //septima encuesta 
            {
                idRespuesta: 49,
                idPregunta: 25,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 50,
                idPregunta: 25,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 51,
                idPregunta: 26,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 52,
                idPregunta: 26,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },

            //octava encuesta 
            {
                idRespuesta: 53,
                idPregunta: 27,
                respuesta: 'respuesta de texto',
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 54,
                idPregunta: 27,
                respuesta: 'respuesta de texto',
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 55,
                idPregunta: 28,
                respuesta: 0,
                esActualizada: 0,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                idRespuesta: 56,
                idPregunta: 28,
                respuesta: 0,
                esActualizada: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ]);
    }
}