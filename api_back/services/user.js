var usuario = require('../models').usuario;
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];

exports.createUser = async function (user) {

    var hashedPassword = bcrypt.hashSync(user.contrasena, 8);

    var newUser = new usuario({
        nombre: user.nombre,
        email: user.email,
        contrasena: hashedPassword,
        rol: user.rol,
        estado: user.estado
    })

    try {
        //console.log(newUser);
        var savedUser = await newUser.save();
        var token = jwt.sign({
            id: savedUser.id
        }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        return token;
    } catch (e) {
        // return a Error message describing the reason 
        console.log(e)
        throw Error("Error: no se pudo crear el usuario");
    }
}


exports.loginUser = async function (user) {

    try {
        // Find the User 
        console.log("login:", user)
        var userDB = await usuario.findOne({
            where: {
                email: user.email,
            }
        });
        var passwordIsValid = bcrypt.compareSync(user.contrasena, userDB.contrasena);
        //console.log("pass: " + passwordIsValid);
        if (!passwordIsValid) {
            throw Error("Email o contraseña invalido")
        }
        var token = jwt.sign({
            id: userDB.id
        }, config.secret, { 
            expiresIn: 86400 // expires in 24 hours
        });
        return { token: token, user: userDB };
    } catch (e) {
        throw(e);
         //throw Error("Error: no se pudo autenticar al usuario");
    }

}