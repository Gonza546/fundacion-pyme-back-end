'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class comentarios extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      comentarios.belongsTo(models.preguntas,{
        as:'preguntas',
        foreignKey:'idPregunta'
      });
      comentarios.belongsTo(models.usuario,{
        as:'usuario',
        foreignKey:'idUsuario'
      });
    }
  };
  comentarios.init({
    idComentario:{
      allowNull: false,
      autoIncrement:true,
      primaryKey:true,
      type: DataTypes.INTEGER
    },
    idPregunta: DataTypes.INTEGER,
    comentario: DataTypes.STRING,
    fecha: DataTypes.DATE,
    idUsuario: DataTypes.INTEGER,
    respuestaActualizada: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'comentarios',
  });
  return comentarios;
};