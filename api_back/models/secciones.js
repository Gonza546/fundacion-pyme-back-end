'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class secciones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      secciones.belongsTo(models.encuestas,{
        as:'encuestas',
        foreignKey:'idEncuesta'
      });
    }
  };
  secciones.init({
    idSeccion:{
      allowNull: false,
      autoIncrement:true,
      primaryKey:true,
      type: DataTypes.INTEGER
    },
    idEncuesta:DataTypes.INTEGER,
    titulo: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'secciones',
  });
  return secciones;
};