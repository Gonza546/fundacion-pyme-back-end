'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class respuestas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      respuestas.belongsTo(models.preguntas,{
        as:'preguntas',
        foreignKey:'idPregunta'
      });
    }
  };
  respuestas.init({
    idRespuesta:{
      allowNull:false,
      autoIncrement:true,
      primaryKey:true,
      type:DataTypes.INTEGER
    },
    idPregunta: DataTypes.INTEGER,
    respuesta: DataTypes.CHAR,
    esActualizada: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'respuestas',
  });
  return respuestas;
};