'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class preguntas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      preguntas.belongsTo(models.encuestas,{
        as:'encuestas',
        foreignKey:'idEncuesta'
      });
      preguntas.belongsTo(models.secciones,{
        as:'secciones',
        foreignKey:'idSeccion'
      });
      preguntas.belongsTo(models.preguntas,{
        as:'preguntas',
        foreignKey:'idPreguntaPadre'
      });
    }
  };
  preguntas.init({
    idPregunta:{
      allowNull: false,
      autoIncrement:true,
      primaryKey:true,
      type: DataTypes.INTEGER
    },
    idSeccion:{
      allowNull: true,
      type: DataTypes.INTEGER
    },
    idEncuesta: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    idPreguntaPadre: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
    type: DataTypes.STRING,
    titulo: DataTypes.STRING,
    mandatory: DataTypes.BOOLEAN,
    valoresPosibles: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'preguntas',
  });
  return preguntas;
};